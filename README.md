# OSEG DIN Guideline - Documentation Example - Pandoc - 1 - Most Simple

Result:
[HTML](https://osegtechdoctemplates.gitlab.io/pandoc_1/) |
[PDF](https://osegtechdoctemplates.gitlab.io/pandoc_1/index.pdf)

This contains just the most simple possible way:

* [x] a single Markdown source file
* [ ] PP (pre-processor) directives
* [x] pandoc
* [ ] jekyll
* [ ] pdsite
* [ ] sphinx
* [x] output: HTML
* [x] output: PDF
